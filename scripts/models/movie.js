'use strict';

let poster_prefix = "http://image.tmdb.org/t/p/w500/";

function MovielModel() {
	this.id = 0;
	this.title = '';
	this.overview = '';
	this.poster_path = '';
}

let toMovieModel = function(movieJson) {
	let movieJS = JSON.parse(JSON.stringify(movieJson));

	let movie = new MovielModel();
	
	movie.id = movieJS.id;
	movie.title = movieJS.title;
	movie.overview = movieJS.overview;
	movie.poster_path = poster_prefix + movieJS.poster_path;
	
	return movie;
};

'use strict';

/* Defining the AngularJS application */

let crossthinkApp = angular.module('crossthinkApp', ['ngRoute', 'ngResource']);


/* Module configuration and routing */
crossthinkApp.config(['$routeProvider', '$locationProvider', function ($routeProvider, $locationProvider) {

    $routeProvider
        .when("/", {
            templateUrl: "home.html"
        })
        .when('/discover-movies',
            {templateUrl: 'discover-movies.html'}
        )
        .when('/movie-list',
            {templateUrl: 'movie-list.html'}
        );
}]);
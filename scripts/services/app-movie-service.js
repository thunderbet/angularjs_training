'use strict';

crossthinkApp.service('MoviesService', function($http, $resource){
    this.getAMovie = function() {
        return "Jurassic Park";
    };

    this.discoverMovie_pureJS = function(callback) {
        $http({
            method: 'GET',
            url: 'https://api.themoviedb.org/3/discover/movie?api_key=4396a2abd282ceb0559f27bf820c4f35&language=en-US&sort_by=popularity.desc&include_adult=false&include_video=false&page=1',
            dataType: 'json',
            headers: {
                'Content-Type': 'application/json',
                'Accept' : 'application/json'
            }
        }).then(function successCallback(response) {
            // Execute la methode de callback
            callback(response);
        }, function errorCallback(response) {
            // Execute la methode de callback
            console.err(response);
        });
    }

    this.discoverMovie = function(callback) {
        let rest_service = $resource('https://api.themoviedb.org/3/discover/movie?api_key=4396a2abd282ceb0559f27bf820c4f35&language=en-US&sort_by=popularity.desc&include_adult=false&include_video=false&page=1');

        rest_service.get().$promise.then(function(response) {
            // Execute la methode de callback
            callback(response);
        }, function(response) {
            console.error(response);
        });
    }
});
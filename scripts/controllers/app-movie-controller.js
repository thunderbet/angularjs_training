'use strict';

crossthinkApp.controller("MovieController", function ($scope, MoviesService) {
    /* Variables */
    $scope.movie = MoviesService.getAMovie();
    $scope.visible = true;
    $scope.movieList = [];
    $scope.poster = "";

    /* Methods*/
    let processResponse = function (response) {
        let results = response.results;
        let i;

        for (i = 0; i < results.length; i++) {
            $scope.movieList[i] = toMovieModel(results[i]);
        }

        let randomMovie = Math.floor(Math.random() * Math.floor(20));

        $scope.movie = $scope.movieList[randomMovie].title;
        $scope.poster = $scope.movieList[randomMovie].poster_path;
    };

    $scope.clear = function () {
        $scope.movie = "";
        console.log('Clear movie');
    };

    $scope.toggleVisible = function () {
        $scope.visible = !$scope.visible;
        console.log('visible toggled');
    };

    $scope.discoverMovie = function () {
        MoviesService.discoverMovie(processResponse);
    };

    $scope.discoverMovie();
});